FROM frooooonk/android-cmdlinetools:latest

ARG VERSION=
ARG ANDROID_SDK_ROOT=
ARG ANDROID_SDK=
ARG ANDROID_BUILD_TOOLS=

# labels
LABEL description = "android-sdk@${ANDROID_SDK} + android-build-tools@${ANDROID_BUILD_TOOLS}"

ENV PATH "$PATH:${ANDROID_SDK_ROOT}/platform-tools/:${ANDROID_SDK_ROOT}/tools/bin/:${ANDROID_SDK_ROOT}/emulator/:${ANDROID_SDK_ROOT}/build-tools/${ANDROID_BUILD_TOOLS}/"

# install android tools and platform
RUN echo y | sdkmanager "platforms;android-${ANDROID_SDK}" > /dev/null
RUN echo y | sdkmanager "platform-tools" > /dev/null
RUN echo y | sdkmanager "emulator" > /dev/null
RUN echo y | sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" > /dev/null

RUN yes | sdkmanager --licenses > /dev/null
